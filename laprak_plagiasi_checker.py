# -*- coding: utf-8 -*-
"""Laprak plagiasi checker

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1hYn5xJkPfZPX5x7Pja-1C-SOVh_XqpF4

### Installing external library
"""

!pip install PyPdf4
!pip install PySastrawi

"""### Exporting required library"""

from google.colab import files
import pandas as pd
import numpy as np
import os
import PyPDF4
import re
import math
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory as swf ## library for stopword removal
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory as sf ## library for stemming

"""### **[User Interaction]** upload the zip containing the student report"""

uploaded = files.upload()

for fn in uploaded.keys():
  print('User uploaded file "{name}" with length {length} bytes'.format(
      name=fn, length=len(uploaded[fn])))

"""### **[User Interaction]** provide the name of the file with the .zip extension that you upload after unzip command


example:
```
!unzip testing.zip
```


"""

!unzip halimah.zip

#example:
# !unzip testing.zip

"""### **[User Interaction]** provide the name of the folder from extracted zip containing all .pdf file inside the "DIRECTORY" variable 

example:
```
DIRECTORY = "folder_name_from_extracted_zip"
```
"""

DIRECTORY = 'halimah'  #fill the directory/folder name from extracted .zip file

"""### Reading all the directory"""

directories = []
for filename in os.listdir(DIRECTORY):
  if filename.endswith(".pdf"): 
    directories.append(os.path.join(DIRECTORY, filename))
  else:
    continue

directories

"""### Creating stopword removal and stemmer factory"""

## creating factory stopword removal dan stemmer
stop_word_factory = swf().create_stop_word_remover()
stemmer_factory = sf().create_stemmer()

"""### Extracting text from all the pdf file"""

allPdfText = {}
for dir in directories:
  filename = dir.split('/')[-1]
  pdfFileObj = open(dir, 'rb')  
  reader = PyPDF4.PdfFileReader(pdfFileObj)
  allPdfText[filename] = []
  for i in range(reader.getNumPages()):
    allPdfText[filename].append(reader.getPage(i).extractText())
  
  allPdfText[filename] = ' '.join(allPdfText[filename])
  allPdfText[filename] = re.sub(r'[^A-Za-z]', ' ', allPdfText[filename]).lower()
  allPdfText[filename] = ' '.join(allPdfText[filename].split())
  allPdfText[filename] = stop_word_factory.remove(allPdfText[filename]) ## stopword removal
  allPdfText[filename] = stemmer_factory.stem(allPdfText[filename]) ##stemming
  allPdfText[filename] = allPdfText[filename].split()

"""### checking the extracted text from pdf, is there pdf thats not extracted"""

for pdf in list(allPdfText):
  if (len(allPdfText[pdf]) == 0):
    print('File '+pdf+" textnya tidak terekstrak")
    print('Menghapus '+pdf+" dari list, untuk memperbaiki akurasi pengecekan...\n")
    del allPdfText[pdf]

"""### Selecting only unique / distinct term"""

### Concatenate array of text and find unique
allTerm = []
for pdf in allPdfText:
  allTerm = np.concatenate((allTerm, allPdfText[pdf]));
allTerm = np.unique(allTerm)

"""### Calculating weight and normalizing each weight"""

### Calculate weight
weight = pd.DataFrame() #create a new DataFrame
weight['term'] = allTerm #
weight.set_index('term', inplace=True)
weight.index.name = None

for pdf in allPdfText:
  weight[pdf] = [allPdfText[pdf].count(term) for term in allTerm] #raw tf
  weight[pdf] = weight[pdf].apply(lambda x: 1+math.log10(x) if x > 0 else 0) #log tf

idf = np.log10(len(weight.columns)/weight.astype(bool).sum(axis=1)) # count inverse document freq

weight = weight.mul(idf, axis="index") # multiply weight on each document with the idf 

# looping normalization
for pdf in allPdfText:
  weight[pdf] = weight[pdf] / np.sqrt(sum(weight[pdf]**2))

"""### Calculating the similiarity between pdf document with cosine similiarity"""

## Cosine Simmiliarity

cosine_sim = pd.DataFrame()
cosine_sim['page'] = weight.columns
cosine_sim.set_index('page', inplace=True)
cosine_sim.index.name = None

for pdf in weight:
  cosine_sim[pdf] = [np.dot(weight[pdf], weight[x]) for x in cosine_sim.index]

"""### **[User Interaction]** provide the threshold value between 0 and 1 to highlight the value (the default value is 0.85) 
example:

```
THRESHOLD = [desired_value_between 0 and 1]
```
"""

THRESHOLD = 0.85

"""### Provide function to highlight the value if >= threshold"""

def highlight_plagiation(s):
    is_plagiation = (s >= THRESHOLD)
    return ['background-color: yellow' if v else '' for v in is_plagiation]

cosine_sim.style.apply(highlight_plagiation)

"""### **[User Interaction]** provide the name of the output you wanted, if omitted the default is named output.xlsx 

example:
```
OUTPUT_NAME = "file_output_you_wanted"
```
"""

OUTPUT_NAME = ""

output = "output.xlsx" if OUTPUT_NAME == "" else OUTPUT_NAME+".xlsx"
cosine_sim.style.apply(highlight_plagiation).to_excel(output)
files.download(output)

